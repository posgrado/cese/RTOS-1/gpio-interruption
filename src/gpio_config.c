/*=====gpio_irq===========================================================
 
 /*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/09
 *===========================================================================*/

/*=====[Inclusion of own header]=============================================*/

#include "gpio_irq.h"
#include "sapi.h"
#include "task.h"

/*=====[Inclusions of private function dependencies]=========================*/

/*=====[Definition macros of private constants]==============================*/

/*=====[Private function-like macros]========================================*/

/*=====[Definitions of private data types]===================================*/

/*=====[Definitions of external public global variables]=====================*/

/*=====[Definitions of public global variables]==============================*/

struct Buttons_SM_t Buttons_SM[CANT_TECLAS];

/*=====[Definitions of private global variables]=============================*/

/*=====[Prototypes (declarations) of external public functions]==============*/

/*=====[Prototypes (declarations) of private functions]======================*/

/*=====[Implementations of public functions]=================================*/
void gpio_IRQ_Init(void) {
	//Inicializamos las interrupciones (LPCopen)
	Chip_PININT_Init(LPC_GPIO_PIN_INT);

	// TEC1 FALL
	Chip_SCU_GPIOIntPinSel(0, 0, 4); 	//(Canal 0 a 7, Puerto GPIO, Pin GPIO)
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH0); //Se configura el canal para que se active por flanco
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH0); //Se configura para que el flanco sea el de bajada

	// TEC1 RISE
	Chip_SCU_GPIOIntPinSel(1, 0, 4);	//(Canal 0 a 7, Puerto GPIO, Pin GPIO)
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH1);//Se configura el canal para que se active por flanco
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH1);	//En este caso el flanco es de subida

	//Una vez que se han configurado los eventos para cada canal de interrupcion
	//Se activan las interrupciones para que comiencen a llamar al handler
	NVIC_SetPriority(PIN_INT0_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT0_IRQn);
}
/*=====[Implementations of interrupt functions]==============================*/

void GPIO0_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE; //Comenzamos definiendo la variable

	if (Chip_PININT_GetFallStates(LPC_GPIO_PIN_INT) & PININTCH0) { //Verificamos que la interrupciÃ³n es la esperada
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH0); //Borramos el flag de interrupciÃ³n
		//codigo a ejecutar si ocurriÃ³ la interrupciÃ³n

		struct Button_Control Snapshot;
		Snapshot.Flanco = FALLING;
		Snapshot.Tiempo_inicial = xTaskGetTickCountFromISR();

		xQueueSendFromISR(Buttons_SM[Tecla1].Cola, &Snapshot,
				&xHigherPriorityTaskWoken);

	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/*=====[Implementations of private functions]================================*/

