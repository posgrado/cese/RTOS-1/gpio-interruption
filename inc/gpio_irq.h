/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/06/09
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef _GPIO_IRQ_H_
#define _GPIO_IRQ_H_

/*=====[Inclusions of public function dependencies]==========================*/

#include "FreeRTOS.h"
#include "queue.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

#define CANT_TECLAS 1
#define FALLING 2

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

struct Buttons_SM_t { //estructura de control de la mÃ¡quina de estados de cada botÃ³n
	uint8_t Tecla;
	uint8_t Estado;xQueueHandle Cola;
	TickType_t Tiempo_inicial;
};

struct Button_Control { //estructura de control de datos capturados por la interrupciÃ³n
	TickType_t Tiempo_inicial;
	uint8_t Flanco;
};

enum Teclas_t {
	Tecla1
};

/*=====[Prototypes (declarations) of public functions]=======================*/

void gpio_IRQ_Init(void);

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

void GPIO0_IRQHandler(void);

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* _GPIO_IRQ_H_ */

